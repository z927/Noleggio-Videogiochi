package com.app.oken.noleggiovideogiochi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity{
    private Button btnLogin, btnSign;
    private EditText txtUser;
    private EditText password;
    public Intent change;
    private DataBase db;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new DataBase(this);
        txtUser = (EditText) findViewById(R.id.txtUser);
        btnSign = (Button) findViewById(R.id.button_sign);
        btnSign.setOnClickListener(new View.OnClickListener(){
            //@Override
            public void onClick(View view){
                //txtUser.setText("prova");
                change = new Intent(MainActivity.this, SignActivity.class);
                startActivity(change);
            }
        });

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener(){
            //@Override
            public void onClick(View view){
                Toast.makeText(getApplicationContext(), "L'utente non risulta nel Database", Toast.LENGTH_SHORT).show();
            }
        });
    }
}