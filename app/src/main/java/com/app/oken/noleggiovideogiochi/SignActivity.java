package com.app.oken.noleggiovideogiochi;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignActivity extends AppCompatActivity {
    Button btnSign;
    EditText txtNome, txtCognome, txtPassword, txtEmail;
    private DataBase db;

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);

        db = new DataBase(this);
        btnSign = (Button) findViewById(R.id.btnSign);
        btnSign.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                register();
            }
        });
        txtNome = (EditText) findViewById(R.id.txtName);
        txtCognome = (EditText) findViewById(R.id.txtCognome);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
    }
    public void register(){
        if(txtNome.length() == 0){
            Toast.makeText(getApplicationContext(), "Inserire un nome utente", Toast.LENGTH_SHORT).show();
        }else if(txtCognome.length() == 0){
            Toast.makeText(getApplicationContext(), "Inserire un cognome utente", Toast.LENGTH_SHORT).show();
        }else if(txtPassword.length() < 8){
            Toast.makeText(getApplicationContext(), "La password deve essere superiore a 7 caratteri", Toast.LENGTH_SHORT).show();
        }else if(isValidEmail(txtEmail.getText()) == false){
            Toast.makeText(getApplicationContext(), "inserire una email valida", Toast.LENGTH_SHORT).show();
        }else{
            db.insertUser(new User(txtNome.getText().toString(), txtCognome.getText().toString(), txtPassword.getText().toString(), txtEmail.getText().toString()));
            //Toast.makeText(getApplicationContext(), "Benvenuto in noleggio videogiochi", Toast.LENGTH_SHORT).show();
            Context ctx = this;
            Toast.makeText(getApplicationContext(), String.valueOf(ctx.getDatabasePath(db.DATABASE_NAME)), Toast.LENGTH_SHORT).show();
        }
    }
    public static boolean isValidEmail(CharSequence target){
        if(target == null){
            return false;
        }else{
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}