package com.app.oken.noleggiovideogiochi;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBase extends SQLiteOpenHelper{
    public static String DATABASE_NAME = "Noleggio_Videogiochi";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "users";
    private static final String KEY_ID = "id";
    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_PASSWORD = "password";

    public DataBase(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db){
        // create books table
        db.execSQL("create table "
                + TABLE_NAME + " ("
                + KEY_ID + " integer primary key autoincrement, "
                + KEY_FIRST_NAME + " TEXT not null, "
                + KEY_LAST_NAME + " TEXT not null, "
                + KEY_EMAIL + " TEXT not null, "
                + KEY_PASSWORD + " TEXT not null"
                + ");");
        Log.d("Prova", "onCreate(): create table");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int j){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        this.onCreate(db);
        Log.d("Prova: ", "onUpgrade(): created fresh table");
    }
    public long insertUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, user.getNome());
        values.put(KEY_LAST_NAME, user.getCognome());
        values.put(KEY_PASSWORD, user.getPassword());
        values.put(KEY_EMAIL, user.getEmail());
        long id = db.insert(TABLE_NAME, null, values); // key/value -> keys = column names/ values = column values
        //Log.d(TAG, "insertItem("+id+") " + item.toString());
        db.close();
        return id;
    }
}