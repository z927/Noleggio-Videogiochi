package com.app.oken.noleggiovideogiochi;


public class User{
    public String nome, cognome, password, email;

    public User(String nome, String cognome, String password, String email){
        this.nome = nome;
        this.cognome = cognome;
        this.password = password;
        this.email = email;
    }
    public String getNome(){
        return this.nome;
    }
    public String getCognome(){
        return this.cognome;
    }
    public String getPassword(){
        return this.password;
    }
    public String getEmail(){
        return this.email;
    }
}